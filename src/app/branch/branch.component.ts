import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  currentBranch = {};
  constructor(private dataService: DataService,
  private route: ActivatedRoute) { }

  ngOnInit() {
    this.getBranchDetail();
  }

  getBranchDetail() {
    const id = +this.route.snapshot.paramMap.get('id');
    let list = [];
    this.dataService.getBranches().subscribe(reponse => {
      list = reponse.data[0].Brand[0].Branch.filter(item => {
        return item.Identification == id;
      });
      this.currentBranch = list[0];
      console.log(this.currentBranch);
    })
  }

}
