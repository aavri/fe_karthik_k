import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BranchesComponent } from './branches/branches.component';
import { BranchComponent } from './branch/branch.component';

const routes: Routes = [
  { path: '', redirectTo: '/branches', pathMatch: 'full' },
  { path: 'branches', component: BranchesComponent },
  { path: 'detail/:id', component: BranchComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}