import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {

  branchList;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getBranches();
  }

  getBranches() {
    this.dataService.getBranches().subscribe(response => {
      this.branchList = response.data[0].Brand[0].Branch;
      console.log(this.branchList);
    });
  }

}
